package vsgridmaps;

import java.io.IOException;

public class VSGridMaps {

    private static String map = "Small/";
    private static String Algorithm = "Euclid";
    private static Integer testSize = 7;

    public static void main(String[] args) throws IOException {
            for(int i = 7; i <= testSize; i++) {
                ProblemInstance problemInst = Load.instance("src/tests/"+map+"\\" + i + ".txt");

                Solver solver = new Solver(problemInst);
                solver.solve(Algorithm);

                Save.solution("src/testsOutput/"+ map + Algorithm +Integer.toString(problemInst.getInstancenumber()) + ".txt", problemInst, 6);
                Load.solution("src/testsOutput/"+ map + Algorithm +Integer.toString(problemInst.getInstancenumber()) + ".txt", problemInst);

                if(problemInst.isValid()){
                    System.out.println("\nSCOOOOOOOORE " + problemInst.computeScore());
                    System.out.println("Solution is valid: " + problemInst.isValid() + "\n");
                }else{
                    System.out.println("SOLUTION NOT VALID STOP!");
                    break;
                }

            }
    }

}
