package vsgridmaps;

import gurobi.*;

import java.util.ArrayList;

public class Solver {

    private ProblemInstance problemInstance;
    private WeightedPointSet weightedPoints;
    final private double MODULOFULL = 0.0;
    final private double MODULOHALF = 0.5;
    private double BIG_M_UPPERBOUND = -1.0;
    private GRBVar[][] manhattenConstrains;
    private ArrayList<GRBVar> cooridnateConstraintVars;
    private GRBVar[][][]constraintValues;

    public Solver(ProblemInstance p){
        this.problemInstance = p;
        weightedPoints = problemInstance.getPointset();
        manhattenConstrains = new GRBVar[weightedPoints.size()][4];
        cooridnateConstraintVars = new ArrayList<>();
        constraintValues = new GRBVar[weightedPoints.size()][weightedPoints.size()][4];
    }


    public void solve(String algorithm){
        try {
            GRBEnv env = new GRBEnv("mip1.log");
            GRBModel model = new GRBModel(env);
            //model.set(GRB.DoubleParam.MIPGap, 0.0000);
            setBIG_M_UPPERBOUND();

            //Create a variable for each point that indicates Distance to assigned point, and assigned point itself
            for (int i = 0; i < weightedPoints.size();i++) {
                WeightedPoint wp = weightedPoints.get(i);
                cooridnateConstraintVars.add(model.addVar(-GRB.INFINITY,GRB.INFINITY,0,GRB.CONTINUOUS,"X for point " +"("+wp.getX()+","+wp.getY()+" Weight: "+wp.getWeight()+")"));
                cooridnateConstraintVars.add(model.addVar(-GRB.INFINITY,GRB.INFINITY,0,GRB.CONTINUOUS,"Y for point " +"("+wp.getX()+","+wp.getY()+" Weight: "+wp.getWeight()+")"));
                createPlacementRestrictions(model, wp, i);
            }

            creatAllConstraints(model);

            if(algorithm.equals("Euclid")){
                squaredEucliDistLP(model);
            }else{
                manhattenDistLP(model);
            }
            model.optimize();

            //Assign the optimized values in the points
            for (int i = 0; i < weightedPoints.size(); i++){
                weightedPoints.get(i).setAssigned(cooridnateConstraintVars.get(i+i).get(GRB.DoubleAttr.X)/1.0, cooridnateConstraintVars.get((i+i)+1).get(GRB.DoubleAttr.X)/1.0);
            }


            // Dispose of model and environment
            model.dispose();
            env.dispose();


        } catch (GRBException e) {
            System.out.println("Error code: " + e.getErrorCode() + ". " +
                    e.getMessage());
        }
    }

    private void createPlacementRestrictions(GRBModel model, WeightedPoint wp, int i) throws GRBException{

            GRBLinExpr moduloExprLHS = new GRBLinExpr();
            GRBLinExpr moduloExprRHS = new GRBLinExpr();

            GRBLinExpr moduloExprLHS2 = new GRBLinExpr();
            GRBLinExpr moduloExprRHS2 = new GRBLinExpr();

            moduloExprLHS.addTerm(1, model.addVar(-GRB.INFINITY, GRB.INFINITY, 0, GRB.INTEGER, "integer muduloFull constraint"));
            moduloExprLHS2.addTerm(1, model.addVar(-GRB.INFINITY, GRB.INFINITY, 0, GRB.INTEGER, "integer muduloFull constraint"));

            if (wp.getWeight() % 2 != 0) {
                moduloExprLHS.addConstant(MODULOHALF);
                moduloExprLHS2.addConstant(MODULOHALF);
            } else {
                moduloExprLHS.addConstant(MODULOFULL);
                moduloExprLHS2.addConstant(MODULOFULL);
            }
            moduloExprRHS.addTerm(1, cooridnateConstraintVars.get(i + i));
            model.addConstr(moduloExprLHS, GRB.EQUAL, moduloExprRHS, "name");

            moduloExprRHS2.addTerm(1, cooridnateConstraintVars.get((i + i) + 1));
            model.addConstr(moduloExprLHS2, GRB.EQUAL, moduloExprRHS2, "name");
            model.update();
    }

    private void manhattenDistLP(GRBModel model) throws GRBException{

        for(int i = 0; i < weightedPoints.size(); i++) {
            manhattenConstrains[i][0] = model.addVar(0,GRB.INFINITY, 0, GRB.CONTINUOUS, "Cooridnate: " + "(" + weightedPoints.get(i).getX() + "," + weightedPoints.get(i).getY() + " variable x1 and point: " + i);
            manhattenConstrains[i][1] = model.addVar(0,GRB.INFINITY, 0, GRB.CONTINUOUS, "Cooridnate: " + "(" + weightedPoints.get(i).getX() + "," + weightedPoints.get(i).getY() + " variable x2 and point: " + i);
            manhattenConstrains[i][2] = model.addVar(0,GRB.INFINITY, 0, GRB.CONTINUOUS, "Cooridnate: " + "(" + weightedPoints.get(i).getX() + "," + weightedPoints.get(i).getY() + " variable y1 and point: " + i);
            manhattenConstrains[i][3] = model.addVar(0,GRB.INFINITY, 0, GRB.CONTINUOUS, "Cooridnate: " + "(" + weightedPoints.get(i).getX() + "," + weightedPoints.get(i).getY() + " variable y2 and point: " + i);
        }

        for(int i = 0; i < weightedPoints.size(); i++) {
            GRBLinExpr lhsX = new GRBLinExpr();
            lhsX.addTerm(1, cooridnateConstraintVars.get(i+i));
            lhsX.addTerm(1, manhattenConstrains[i][0]);
            GRBLinExpr rhsX = new GRBLinExpr();
            rhsX.addTerm(1, manhattenConstrains[i][1]);
            rhsX.addConstant(weightedPoints.get(i).getX());

            model.addConstr(lhsX, GRB.EQUAL, rhsX, "X constraint for point: "+ i);

            GRBLinExpr lhsY = new GRBLinExpr();
            lhsY.addTerm(1, cooridnateConstraintVars.get((i+i)+1));
            lhsY.addTerm(1, manhattenConstrains[i][2]);
            GRBLinExpr rhsY = new GRBLinExpr();
            rhsY.addTerm(1, manhattenConstrains[i][3]);
            rhsY.addConstant(weightedPoints.get(i).getY());

            model.addConstr(lhsY, GRB.EQUAL, rhsY, "Y constraint for point: "+ i);

            model.update();
        }

        GRBLinExpr expr = new GRBLinExpr();
        for(GRBVar[] optimizeArray: manhattenConstrains) {
            for (GRBVar optimizeVar : optimizeArray) {
                expr.addTerm(1, optimizeVar);
            }
        }

        model.setObjective(expr, GRB.MINIMIZE);
        model.update();
    }

    private void squaredEucliDistLP(GRBModel model) throws GRBException{

        GRBQuadExpr grbQuadExpr = new GRBQuadExpr();
        GRBQuadExpr grbQuadExpr2 = new GRBQuadExpr();

        for(int i = 0; i < weightedPoints.size(); i++) {
            GRBLinExpr exprX = new GRBLinExpr();
            GRBLinExpr exprY = new GRBLinExpr();
            grbQuadExpr.addTerm(1,cooridnateConstraintVars.get((i+i)),cooridnateConstraintVars.get((i+i)));
            exprX.addTerm(-2*weightedPoints.get(i).getX(),cooridnateConstraintVars.get((i+i)));
            exprX.addConstant(weightedPoints.get(i).getX()*weightedPoints.get(i).getX());
            grbQuadExpr.add(exprX);

            grbQuadExpr2.addTerm(1,cooridnateConstraintVars.get((i+i)+1),cooridnateConstraintVars.get((i+i)+1));
            exprY.addTerm(-2*weightedPoints.get(i).getY(),cooridnateConstraintVars.get((i+i)+1));
            exprY.addConstant(weightedPoints.get(i).getY()*weightedPoints.get(i).getY());
            grbQuadExpr2.add(exprY);
            grbQuadExpr.add(grbQuadExpr2);

        }
        model.update();
        model.setObjective(grbQuadExpr,GRB.MINIMIZE);

    }

    private void creatAllConstraints(GRBModel model) throws GRBException{
        //Create overlap constraints for every pair i,j squares
        for(int i = 0; i < weightedPoints.size(); i++) {
            for (int j = 0; j < weightedPoints.size(); j++) {
                double totalWeight = (weightedPoints.get(i).getWeight() + weightedPoints.get(j).getWeight())/2.0;

                if(j < i) {
                    constraintValues[i][j][0] = model.addVar(0, 1, 0, GRB.BINARY, "constraint for comparion squares nr:" + i + " " + j);
                    constraintValues[i][j][1] = model.addVar(0, 1, 0, GRB.BINARY, "constraint for comparion squares nr:" + i + " " + j);
                    constraintValues[i][j][2] = model.addVar(0, 1, 0, GRB.BINARY, "constraint for comparion squares nr:" + i + " " + j);
                    constraintValues[i][j][3] = model.addVar(0, 1, 0, GRB.BINARY, "constraint for comparion squares nr:" + i + " " + j);
                    model.update();

                    //Inequalty M*Uab + M*Vab + x1 = (w1 + w2)/2 + x2
                    GRBLinExpr expr1LHS = new GRBLinExpr();
                    GRBLinExpr expr1RHS = new GRBLinExpr();
                    expr1LHS.addTerm(BIG_M_UPPERBOUND, constraintValues[i][j][0]);
                    expr1LHS.addTerm(1, cooridnateConstraintVars.get((i+i)));

                    expr1RHS.addConstant(totalWeight);
                    expr1RHS.addTerm(1, cooridnateConstraintVars.get((j+j)));
                    model.addConstr(expr1LHS, GRB.GREATER_EQUAL,expr1RHS, "Constr for" + Math.random());

                    //Inequalty M(1-Uab) + M*Vab + x2 = (w1 + w2)/2 + x1
                    GRBLinExpr expr3LHS = new GRBLinExpr();
                    GRBLinExpr expr3RHS = new GRBLinExpr();
                    expr3LHS.addTerm(BIG_M_UPPERBOUND, constraintValues[i][j][1]);
                    expr3LHS.addTerm(1, cooridnateConstraintVars.get((j+j)));

                    expr3RHS.addConstant(totalWeight);
                    expr3RHS.addTerm(1, cooridnateConstraintVars.get(i+i));
                    model.addConstr(expr3LHS, GRB.GREATER_EQUAL, expr3RHS, "Constr for" + Math.random());

                    //Inequalty M*Uab + y1 = (w1 + w2)/2 + y2
                    GRBLinExpr expr5LHS = new GRBLinExpr();
                    GRBLinExpr expr5RHS = new GRBLinExpr();
                    expr5LHS.addTerm(BIG_M_UPPERBOUND, constraintValues[i][j][2]);
                    expr5LHS.addTerm(1, cooridnateConstraintVars.get((i+i)+1));

                    expr5RHS.addConstant(totalWeight);
                    expr5RHS.addTerm(1, cooridnateConstraintVars.get((j+j)+1));
                    model.addConstr(expr5LHS, GRB.GREATER_EQUAL, expr5RHS, "Constr for" + Math.random());

                    //Inequalty M*Uab + y2 = (w1 + w2)/2 + y1
                    GRBLinExpr expr7LHS = new GRBLinExpr();
                    GRBLinExpr expr7RHS = new GRBLinExpr();
                    expr7LHS.addTerm(BIG_M_UPPERBOUND, constraintValues[i][j][3]);
                    expr7LHS.addTerm(1, cooridnateConstraintVars.get((j+j)+1));

                    expr7RHS.addConstant(totalWeight);
                    expr7RHS.addTerm(1, cooridnateConstraintVars.get((i+i)+1));
                    model.addConstr(expr7LHS, GRB.GREATER_EQUAL, expr7RHS, "Constr for" + Math.random());



                    GRBLinExpr expr9LHS = new GRBLinExpr();
                    for(GRBVar var: constraintValues[i][j]) {
                        expr9LHS.addTerm(1,var);
                    }
                    model.addConstr(3, GRB.GREATER_EQUAL, expr9LHS, "name");
                    model.update();
                }
            }
        }
    }

    private void setBIG_M_UPPERBOUND(){
        for(WeightedPoint wp: weightedPoints){
            BIG_M_UPPERBOUND += wp.getWeight();
        }
    }

    private void printDebug() throws GRBException{
        for(GRBVar coordvar: cooridnateConstraintVars) {
            System.out.println(coordvar.get(GRB.StringAttr.VarName)
                    + " " + coordvar.get(GRB.DoubleAttr.X));
        }

//            for(GRBVar[][] add1: constraintValues) {
//                for (GRBVar[] add2 : add1) {
//                    for (GRBVar coordvar : add2) {
//                        if(coordvar != null) {
//                            System.out.println(coordvar.get(GRB.StringAttr.VarName)
//                                    + " " + coordvar.get(GRB.DoubleAttr.X));
//                        }
//                    }
//                }
//            }

//            for(GRBVar[] constr: manhattenConstrains) {
//                for(GRBVar constrVal: constr) {
//                        if(constrVal != null) {
//                            System.out.println(constrVal.get(GRB.StringAttr.VarName)
//                                    + " " + constrVal.get(GRB.DoubleAttr.X));
//                    }
//                }
//            }
    }


}
